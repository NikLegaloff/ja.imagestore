## Setup solution

### Edit file \JA.ImageStore\JA.ImagesHost\Domain\Env.cs and set the follow things

- connection string per environment (all tables will be added to db at first start)
- images path for environments (for example c:\img\)

### Setup IIS, point it to \JA.ImagesHost\ and add vitual dir \Img\  and point it to images folder (for example c:\img\)

## Ussage 
   
    maxImageWidth=800;
    var client = new ImagesHostClient("http://localhost:9011/", contextId);
    Folder[] foldersTree = client.GetFoldersTree();
    foreach (var file in Directory.GetFiles("O:\\1\\"))
    {
        var bytes = File.ReadAllBytes(file);
        FileInfo f = new FileInfo(file);
        var imageId = client.UploadImage(f.Name, bytes, foldersTree[0].Id);
        var url = client.GetImageUrl(imageId, maxImageWidth)
    }
    // cleanup - delete images with custom max width, run daily
    ImagesHostClient.Cleanup(imagesBasePath, maxDaysCacheLifetime);

## other