using System;

namespace JA.ImagesHost.Domain
{
    public class FolderDTO
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
    }

    public class ImageDTO
    {
        public Guid Id { get; set; }
        public Guid? FolderId { get; set; }
        public string Name { get; set; }
        public Guid Hash { get; set; }
        public int Size { get; set; }
    }
}