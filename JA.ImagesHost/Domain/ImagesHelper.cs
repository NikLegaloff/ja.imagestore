using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace JA.ImagesHost.Domain
{
    public class ImagesHelper
    {
        private readonly Guid _contextId;

        public ImagesHelper(Guid contextId)
        {
            _contextId = contextId;
        }

        public void DeleteImageFile(Guid id)
        {
            var files = Directory.GetFiles(GetFolderPath(id));
            int deleted = 0;
            foreach (var file in files)
            {
                if (file.Contains(id.ToString()))
                {
                    File.Delete(file);
                    deleted++;
                }
            }
            if (deleted==files.Length) Directory.Delete(GetFolderPath(id));
        }

        public string GetFolderPath(Guid id)
        {
            return $"{Env.Current.ImagesBasePath}{_contextId}\\{id.ToString().Substring(0, 2)}\\{id.ToString().Substring(2, 2)}\\";

        }

        public string GetImagePath(Guid id)
        {
            return $"{GetFolderPath(id)}{id}.jpg";
        }

        public string GetImageMaxWidthPath(Guid id, int maxWidth)
        {
            return $"{GetFolderPath(id)}{id}_X{maxWidth}.jpg";
        }

        public string GetTmbPath(Guid id)
        {
            return $"{GetFolderPath(id)}{id}_tmb.jpg";
        }

        public Guid UploadFile(string name, byte[] data, Guid? folderId)
        {

            var hash = GetHash(data);
            var size = data.Length;
            var existing =
                DB.ExecuteScalar<Guid?>(
                    "select id from image where contextId=@contextId and hash=@hash and size=@size",
                    new {hash, size, contextId = _contextId});
            if (existing != null) return existing.Value;
            var id = Guid.NewGuid();
            var folderPath = GetFolderPath(id);
            if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);
            Resize(data, id);
            DB.Execute(
                "insert into Image(id,name,folderId,hash,size, contextId) values(@id,@name,@folderId,@hash,@size,@contextId)",
                new {id, name, folderId, hash, size, contextId = _contextId});
            return id;
        }

        private void Resize(byte[] data, Guid id)
        {
            if (File.Exists(GetImagePath(id))) File.Delete(GetImagePath(id));
            if (File.Exists(GetTmbPath(id))) File.Delete(GetTmbPath(id));
            using (var ms = new MemoryStream(data))
            {
                using (var g = System.Drawing.Image.FromStream(ms))
                {
                    Draw(data, GetImagePath(id), Env.Current.MaxWidthImg, g);
                    Draw(data, GetTmbPath(id), Env.Current.MaxWidthTmb, g);
                }
            }
        }

        private static void Draw(byte[] data, string path, int maxWidth, System.Drawing.Image g)
        {
            if (g.Width > maxWidth)
            {
                var thumbSize = new Size(g.Width, g.Height);

                if (thumbSize.Width > maxWidth)
                    thumbSize = new Size(maxWidth, thumbSize.Height * maxWidth / thumbSize.Width);
                using (var imgOutput = new Bitmap(thumbSize.Width, thumbSize.Height))
                {
                    imgOutput.MakeTransparent();
                    imgOutput.MakeTransparent(Color.Black);
                    using (var newGraphics = Graphics.FromImage(imgOutput))
                    {
                        newGraphics.CompositingQuality = CompositingQuality.HighQuality;
                        newGraphics.InterpolationMode = InterpolationMode.Bicubic;
                        newGraphics.Clear(Color.FromArgb(255, 255, 255, 255));
                        newGraphics.DrawImage(g, 0, 0, thumbSize.Width, thumbSize.Height);
                        newGraphics.Flush();
                    }
                    var parameters = new EncoderParameters(1);
                    var parameter = new EncoderParameter(Encoder.Quality, 75L);
                    parameters.Param[0] = parameter;
                    var codec = GetImageCodecInfo(ImageFormat.Jpeg);
                    imgOutput.Save(path, codec, parameters);
                }
            }
            else
            {
                File.WriteAllBytes(path, data);
            }
        }

        private static ImageCodecInfo GetImageCodecInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageEncoders().FirstOrDefault(codec => codec.FormatID == format.Guid);
        }

        private Guid GetHash(byte[] data)
        {
            using (var md5Hasher = MD5.Create())
            {
                return new Guid(md5Hasher.ComputeHash(data));
            }
        }

        public Stream GetResizedImage(Guid id, int maxWidth)
        {
            if (File.Exists(GetImageMaxWidthPath(id, maxWidth))) return File.OpenRead(GetImageMaxWidthPath(id, maxWidth));
            if (!File.Exists(GetImagePath(id))) return null;
            var data = File.ReadAllBytes(GetImagePath(id));
            using (var ms = new MemoryStream(data))
            {
                using (var g = System.Drawing.Image.FromStream(ms))
                {
                    Draw(data, GetImageMaxWidthPath(id, maxWidth), maxWidth, g);
                    return File.OpenRead(GetImageMaxWidthPath(id, maxWidth));
                }
            }
        }
    }
}