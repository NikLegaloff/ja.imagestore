﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using JA.ImagesHost.Domain;

namespace JA.ImagesHost
{
    public class ImageController : ApiController
    {
        // GET api/<controller>
        public HttpResponseMessage Get( Guid contextId, Guid id, int maxWidth)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var helper = new ImagesHelper(contextId);
            var stream = helper.GetResizedImage(id, maxWidth);
            if (stream==null) return new HttpResponseMessage(HttpStatusCode.NotFound);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
            return result;
        }
    }
}