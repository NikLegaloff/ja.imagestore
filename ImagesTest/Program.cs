﻿using System;
using System.IO;
using System.Net;
using JA.ImagesClient;

namespace ImagesTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ImagesHostClient.Cleanup("C:\\Img\\", 0);
            var client = new ImagesHostClient("http://localhost:9011/", Guid.Empty);
            var folder1Id =  client.AddFolder("Folder1", null);
            client.RenameFolder(folder1Id, "FolderNewName");
            var folder2Id = client.AddFolder("Folder2", folder1Id);
            var image1 = client.UploadImage("File1.jpg", File.ReadAllBytes("O:\\1\\1.jpg"), folder2Id);
            for (int i = 100; i <= 1600; i+=100)
            {
                var n = DateTime.Now;
                var url = client.GetImageUrl(image1, i);
                new WebClient().DownloadData(url);
                var n2 = DateTime.Now;
                new WebClient().DownloadData(url);

                Console.WriteLine( i + "\t" + n2.Subtract(n).TotalMilliseconds + "\t" + DateTime.Now.Subtract(n2).TotalMilliseconds);
            }
            
            client.RenameImage(image1, "FileRenamed.jpg");
            client.DeleteFolder(folder2Id,folder1Id);
            var tree = client.GetFoldersTree();
            Console.WriteLine(tree.Length);
            Console.ReadLine();
            var id = tree[0].Id;
            client.DeleteFolder(id,null);
            client.DeleteImage(image1);
        }
    }
}
